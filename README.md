# HICX Interview Exercise

Brief explanation of the tools created:

- FileStatistic - Entity that holds the data.
- FileParser - Applies the statistics processors to the files on the give directory.


- StatisticProcessor - Defines the operations for calculating statistics for a given file.
- StatisticProcessorDotsCounter - implementation which counts the dots.
- StatisticProcessorWordsCounter - implementation which counts the words.
- StatisticProcessorMostUsedWord - implementation which finds the most used word.

The supported extensions are defined for each implementation.

After a file was processed by at least on statistic processor it will be moved to a proccessed directory.

Three files where created on the resources folder that can be used for testing.

I ran out of time and did not add any tests or logger.

## **Instructions**


### *Compiling your code*
To compile your application run:

```
mvn clean install 
```

### *Execute your code*
To execute your application run:  
(replace the first argument with the directory where your files are)
```
mvn compile exec:java -Dexec.mainClass="com.hicx.interview.EntryPoint" -Dexec.arguments="C:\\a2b-development\\git-repositories\\hicx\\interview-exercise\\src\\main\\resources"
```
