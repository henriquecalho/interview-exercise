package com.hicx.interview.statistic;

import com.hicx.interview.model.FileExtensionEnum;
import com.hicx.interview.model.FileStatistic;
import com.hicx.interview.model.FileStatisticTypeEnum;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class StatisticProcessorWordsCounter extends BaseStatisticProcessor {

    public StatisticProcessorWordsCounter(List<FileExtensionEnum> supportedExtensions) {
        super(supportedExtensions);
    }

    public FileStatistic execute(Path path) throws FileNotFoundException {

        File file = path.toFile();
        int words = 0;
        try(Scanner sc = new Scanner(new FileInputStream(file))){
            while(sc.hasNext()){
                sc.next();
                ++words;
            }
        }

        Map<String, String> statistics = new HashMap<>();
        statistics.put("Number of words", String.valueOf(words));

        return new FileStatistic()
                .setPath(path)
                .setFileName(path.getFileName().toString())
                .setFileStatisticType(FileStatisticTypeEnum.WORDS_COUNTER)
                .setStatistics(statistics);
    }
}
