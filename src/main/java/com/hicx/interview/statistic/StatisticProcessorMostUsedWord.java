package com.hicx.interview.statistic;

import com.hicx.interview.model.FileExtensionEnum;
import com.hicx.interview.model.FileStatistic;
import com.hicx.interview.model.FileStatisticTypeEnum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class StatisticProcessorMostUsedWord extends BaseStatisticProcessor {

    public StatisticProcessorMostUsedWord(List<FileExtensionEnum> supportedExtensions) {
        super(supportedExtensions);
    }

    public FileStatistic execute(Path path) throws IOException {

        File file = path.toFile();
        String line;
        String word = "";
        int count;
        int maxCount = 0;
        List<String> words = new ArrayList<>();

        //Opens file in read mode
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)
        ){

            //Reads each line
            while((line = bufferedReader.readLine()) != null) {

                // Regex means:
                // 1 - Zero or more whitespaces (\\s*)
                // 2 - Arrow, or comma, or whitespace (=>|,|\\s)
                // 3 - Zero or more whitespaces (\\s*)
                String[] string = line.toLowerCase().split("\\s*(=>|,|\\s)\\s*");

                //Adding all words generated in previous step into words
                Collections.addAll(words, string);
            }

            //Determine the most repeated word in a file
            for(int i = 0; i < words.size(); i++){
                count = 1;
                //Count each word in the file and store it in variable count
                for(int j = i+1; j < words.size(); j++){
                    if(words.get(i).equals(words.get(j))){
                        count++;
                    }
                }
                //If maxCount is less than count then store value of count in maxCount
                //and corresponding word to variable word
                if(count > maxCount){
                    maxCount = count;
                    word = words.get(i);
                }
            }
        }

        Map<String, String> statistics = new HashMap<>();
        statistics.put("Most used word", word);
        statistics.put("Number of occurrences", String.valueOf(maxCount));

        return new FileStatistic()
                .setPath(path)
                .setFileName(path.getFileName().toString())
                .setFileStatisticType(FileStatisticTypeEnum.MOST_USED_WORD)
                .setStatistics(statistics);
    }
}
