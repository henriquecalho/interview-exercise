package com.hicx.interview.statistic;

import com.hicx.interview.model.FileStatistic;

import java.io.IOException;
import java.nio.file.Path;

public interface StatisticProcessor {

    boolean validate(Path path);

    FileStatistic execute(Path path) throws IOException;
}
