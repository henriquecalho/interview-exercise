package com.hicx.interview.statistic;

import com.hicx.interview.model.FileExtensionEnum;
import com.hicx.interview.model.FileStatistic;
import com.hicx.interview.model.FileStatisticTypeEnum;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatisticProcessorDotsCounter extends BaseStatisticProcessor {

    public StatisticProcessorDotsCounter(List<FileExtensionEnum> supportedExtensions) {
        super(supportedExtensions);
    }

    public FileStatistic execute(Path path) throws IOException {

        File file = path.toFile();
        int dots = 0;
        int i;

        try (FileReader fileReader = new FileReader(file)) {
            while ((i=fileReader.read()) != -1){
                if(i == 46) ++dots;
            }
        }

        Map<String, String> statistics = new HashMap<>();
        statistics.put("Number of dots", String.valueOf(dots));

        return new FileStatistic()
                .setPath(path)
                .setFileName(path.getFileName().toString())
                .setFileStatisticType(FileStatisticTypeEnum.DOTS_COUNTER)
                .setStatistics(statistics);
    }
}
