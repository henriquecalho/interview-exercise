package com.hicx.interview.statistic;

import com.hicx.interview.model.FileExtensionEnum;

import java.nio.file.Path;
import java.util.List;

public abstract class BaseStatisticProcessor implements StatisticProcessor {

    protected List<FileExtensionEnum> supportedExtensions;

    public BaseStatisticProcessor(List<FileExtensionEnum> supportedExtensions){
        this.supportedExtensions = supportedExtensions;

    }

    @Override
    public boolean validate(Path path) {
        for(FileExtensionEnum fileExtensionEnum : supportedExtensions){
            if(path.toString().toLowerCase().endsWith("." + fileExtensionEnum.toString().toLowerCase()))
                return true;
        }
        return false;
    }
}
