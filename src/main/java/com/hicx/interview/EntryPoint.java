package com.hicx.interview;

import com.hicx.interview.model.FileExtensionEnum;
import com.hicx.interview.file.FileParser;
import com.hicx.interview.model.FileStatistic;
import com.hicx.interview.statistic.StatisticProcessor;
import com.hicx.interview.statistic.StatisticProcessorDotsCounter;
import com.hicx.interview.statistic.StatisticProcessorMostUsedWord;
import com.hicx.interview.statistic.StatisticProcessorWordsCounter;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class EntryPoint {

    public static void main(String[] args){

        if(args.length == 0){
            System.out.println("No directory given.");
            return;
        }

        String relativePath = args[0];
        //relativePath = "C:\\a2b-development\\git-repositories\\hicx\\interview-exercise\\src\\main\\resources";

        List<FileExtensionEnum> supportedExtensions = Collections.singletonList(FileExtensionEnum.TXT);
        StatisticProcessor wordsCounter = new StatisticProcessorWordsCounter(supportedExtensions);
        StatisticProcessor dotsCounter = new StatisticProcessorDotsCounter(supportedExtensions);
        StatisticProcessor mostUsedWord = new StatisticProcessorMostUsedWord(supportedExtensions);

        FileParser fileParser = new FileParser(relativePath);
        try {
            List<FileStatistic> fileStatistics = fileParser.process(wordsCounter, dotsCounter, mostUsedWord);
            printResults(fileStatistics);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printResults(List<FileStatistic> fileStatisticList){
        StringBuilder builder = new StringBuilder();
        for(FileStatistic fileStatistic : fileStatisticList){
            builder.append(fileStatistic).append(System.lineSeparator());
        }
        System.out.println(builder.toString());
    }

}
