package com.hicx.interview.file;

import com.hicx.interview.model.FileStatistic;
import com.hicx.interview.statistic.StatisticProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileParser {

    private static final String PROCESSED_FOLDER = "processed";
    private final String relativePath;

    public FileParser(String relativePath){
        this.relativePath = relativePath;
    }

    public List<FileStatistic> process(StatisticProcessor... statisticProcessors) throws IOException {

        List<Path> pathList;
        List<FileStatistic> fileStatistics = new ArrayList<>();

        try (Stream<Path> paths = Files.walk(Paths.get(relativePath), 1)) {
            pathList = paths
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        }

        if(pathList.isEmpty()){
            System.out.println("No files found.");
            return Collections.emptyList();
        }

        for(Path path : pathList){
            for(StatisticProcessor statisticProcessor : statisticProcessors){
                if(statisticProcessor.validate(path)){
                    FileStatistic fileStatistic = tryProcessStatistics(statisticProcessor, path);
                    fileStatistics.add(fileStatistic);
                }
            }
        }

        if(fileStatistics.isEmpty()){
            System.out.println("No files found for the supported extensions.");
            return Collections.emptyList();
        }

        moveToProcessed(fileStatistics);
        return fileStatistics;
    }

    private FileStatistic tryProcessStatistics(StatisticProcessor statisticProcessor, Path path){
        try {
            return statisticProcessor.execute(path);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void moveToProcessed(List<FileStatistic> fileStatistics) {
        String targetFolder = relativePath + File.separator + PROCESSED_FOLDER;
        File dir = new File(targetFolder);
        if (!dir.exists()) dir.mkdirs();

        for(FileStatistic fileStatistic : fileStatistics){

            Path path = fileStatistic.getPath();
            boolean success = path.toFile().renameTo(new File(targetFolder + File.separator + path.getFileName()));
            if(success)
                fileStatistic.setPath(Paths.get(targetFolder));
        }
    }

    private void moveToProcessed(Path path) {
        String targetFolder = relativePath + File.separator + PROCESSED_FOLDER;
        File dir = new File(targetFolder);
        if (!dir.exists()) dir.mkdirs();

        path.toFile().renameTo(new File(targetFolder + File.separator + path.getFileName()));
    }
}
