package com.hicx.interview.model;

public enum FileStatisticTypeEnum {

    WORDS_COUNTER,
    MOST_USED_WORD,
    DOTS_COUNTER
}
