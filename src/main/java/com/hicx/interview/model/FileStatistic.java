package com.hicx.interview.model;

import java.nio.file.Path;
import java.util.Map;

public class FileStatistic {

    private Path path;
    private String fileName;
    private FileStatisticTypeEnum fileStatisticType;
    private Map<String, String> statistics;

    @Override
    public String toString() {
        return "FileStatistic{" + System.lineSeparator() +
                "relativePath=" + path + System.lineSeparator() +
                ", fileName='" + fileName + '\'' + System.lineSeparator() +
                ", fileStatisticType=" + fileStatisticType + System.lineSeparator() +
                ", statistics=" + statistics + System.lineSeparator() +
                '}';
    }

    public Path getPath() { return path; }

    public FileStatistic setPath(Path path) { this.path = path; return this; }

    public String getFileName() { return fileName; }

    public FileStatistic setFileName(String fileName) { this.fileName = fileName; return this; }

    public FileStatisticTypeEnum getFileStatisticType() { return fileStatisticType; }

    public FileStatistic setFileStatisticType(FileStatisticTypeEnum fileStatisticTypeEnum) { this.fileStatisticType = fileStatisticTypeEnum ; return this; }

    public Map<String, String> getStatistics() { return statistics; }

    public FileStatistic setStatistics(Map<String, String> statistics) { this.statistics = statistics; return this; }
}
